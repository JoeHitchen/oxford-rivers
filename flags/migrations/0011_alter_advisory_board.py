# Generated by Django 4.2 on 2023-12-08 22:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flags', '0010_alter_status_flag'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='board',
            field=models.CharField(choices=[('NONE', 'No advisory'), ('INC', 'Caution (Increasing)'), ('DEC', 'Caution (Decreasing)'), ('RED', 'Danger'), ('OUT', 'Outage')], max_length=4),
        ),
    ]
