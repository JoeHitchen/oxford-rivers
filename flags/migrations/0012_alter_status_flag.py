# Generated by Django 4.2 on 2024-05-05 13:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flags', '0011_alter_advisory_board'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='flag',
            field=models.CharField(choices=[('X', 'Grey'), ('G', 'Green'), ('L', 'Light Blue'), ('B', 'Blue'), ('A', 'Amber'), ('R', 'Red'), ('D', 'Black'), ('O', 'Outage')], max_length=1),
        ),
    ]
