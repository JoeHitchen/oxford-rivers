# Generated by Django 3.1.5 on 2021-01-27 20:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flags', '0006_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='flag',
            field=models.CharField(choices=[('X', 'Grey'), ('G', 'Green'), ('B', 'Blue'), ('A', 'Amber'), ('R', 'Red'), ('D', 'Black')], max_length=1),
        ),
    ]
